#!/usr/bin/env bash

iter=2
delay=2


top -b -n "${iter}" -d "${delay}" -o "+%CPU" \
    | grep -A 7 -E '^top -' \
    | tail -1 \
    | awk '{print "cpu%",$9,$NF}' &

top -b -n "${iter}" -d "${delay}" -o "+%MEM" \
    | grep -A 7 -E '^top -' \
    | tail -1 \
    | awk '{print "mem%",$10,$NF}' &

