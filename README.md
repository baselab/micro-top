## Micro-Top

A tiny tiny tiny top suitable for panels (eg. Xfce panel via "Generic Monitor" applet)

Preview:

![microtop](./micro-top.png)
